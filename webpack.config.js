var config = process.env.NODE_ENV === 'production' ? require('./webpack.production.config.js') : require('./webpack.dev.config.js');
console.log("Running webpack in " + process.env.NODE_ENV);
module.exports = config;
