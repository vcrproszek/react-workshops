import alt from '../alt';
import TaskSource from 'sources/TaskSource';

class TaskActions {
  updateTasks(tasks) {
    this.dispatch(tasks);
  }
  fetchTasks() {
    // we dispatch an event here so we can have "loading" state.
    this.dispatch();
    TaskSource.fetch()
      .then((payload) => {
        // we can access other actions within our action through `this.actions`
        this.actions.updateTasks(payload.tasks);
      })
      .catch((errorMessage) => {
        this.actions.tasksFailed(errorMessage);
      });
  }
  tasksFailed(errorMessage) {
    this.dispatch(errorMessage);
  }
}

export default alt.createActions(TaskActions);
