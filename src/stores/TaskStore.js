import alt from '../alt';
import TaskActions from 'actions/TaskActions';

class TaskStore {
  constructor() {
    // below properties are state , ex: TaskStore.getState().tasks
    this.tasks = [];

    this.bindListeners({
      handleFetchTasks: TaskActions.FETCH_TASKS,
    })

    // this.exportPublicMethods({
    // })
  }

  handleFetchTasks() {
    this.tasks = []
  }

}

export default alt.createStore(TaskStore, 'TaskStore');
