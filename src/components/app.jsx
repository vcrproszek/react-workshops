import React from 'react';
import { RouteHandler } from 'react-router';
import Menu from 'components/menu'

export default class App extends React.Component {
  render() {
    return (
      <div className="container-fluid">
        <Menu/>
        <div id="main">
          {this.props.children}
        </div>
      </div>
    );
  }
}
