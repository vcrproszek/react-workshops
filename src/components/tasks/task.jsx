import React, { PropTypes } from 'react';

class Task extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
    this.state.active = false
  }

  componentDidMount() {
  }

  componentWillUnmount() {
  }

  shouldComponentUpdate(nextProps, nextState) {

    return true
  }

  render() {
    // extract data from props/state
    const { task } = this.props
    const { active } = this.state

    let subTasks = undefined, className = active ? 'name active' : 'active'

    return (
      <div>
        <div className="list-group-item">
          <div className={className}>
            <a>{DEV ? task.id : ''} {task.name}</a>
          </div>
        </div>
        {subTasks}
      </div>
    )
  }
}


Task.propTypes = {
}

export default Task;
