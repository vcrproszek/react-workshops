// Bootstrapping module
import React from 'react';
import Router from 'react-router';

//can be moved to .base.config.js if you feel the need
import 'styles/font-awesome/font-awesome.scss'

// Setup root for dynamic child routing
const rootRoute = {
    childRoutes: [{
        path: '/',
        component: require('./components/app'),
        childRoutes: [
            require('./components/tasks')
        ]
    }]
}

React.render(<Router>{rootRoute}</Router>, document.body)
