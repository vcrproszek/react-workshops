// Global hint: "Keep in mind that you don’t need to write pure JSON into the configuration. Use any JavaScript you want. It’s just a node.js module…"
var path               = require('path'),
    srcPath            = path.join(__dirname, 'src'),
    distPath           = path.join(__dirname, 'dist'),
    CommonsChunkPlugin = require("webpack/lib/optimize/CommonsChunkPlugin"),
    NoErrorsPlugin     = require("webpack/lib/NoErrorsPlugin"),
    DefinePlugin       = require("webpack/lib/DefinePlugin"),
    HtmlWebpackPlugin  = require('html-webpack-plugin'),
    ProvidePlugin      = require("webpack/lib/ProvidePlugin")
    ENV_TYPE           = process.env.NODE_ENV;

module.exports = {
  //Sample: Entry as a single chunk
  // entry: ["./global.js", "./app.js"], //get those 2 files and output them as bundle.js
  //Entry as multi chunks
  entry: {
    // Separate entries into chunks
    // signup bundle for signup page/app
    // main bundle for main app
    // common for vendor libraries
    main: path.join(srcPath, 'main.jsx'),
    common: ['bootstrap-sass!./bootstrap-sass.config.js', 'react', 'react-router'] //common libs: example of global.js,react, etc vendors
  },
  output: {
    path: distPath, // output entry to ./dist
    filename: "[name].bundle.js", //[name] is a chunk name from entry object properties
    pathInfo: true,
    publicPath: ''
  },
  module: {
    // preLoaders: [
    //   {test: /\.js$/, loader: 'eslint-loader', exclude: /node_modules/} //config at .eslintrc
    // ],
    loaders: [
      {
        test: /\.scss$/,
        loader: 'style!css!sass?outputStyle=expanded'
        // sass loader, more info: https://github.com/jtangelder/sass-loader
      },
      // **IMPORTANT** This is needed so that each bootstrap js file required by
      // bootstrap-sass-loader has access to the jQuery object
      { test: /bootstrap-sass\/assets\/javascripts\//, loader: 'imports?jQuery=jquery' },
      // { test: /\.scss$/, loader: "style!css!sass?outputStyle=expanded" },

      // ToDo: custom path and source map option did not work
      //{ test: /\.scss$/,
      //  loader: "style!css!sass?outputStyle=expanded&sourceMap=true&includePaths[]=" + bootstrapPathStylesheets },

      // Needed for the css-loader when [bootstrap-sass-loader](https://github.com/justin808/bootstrap-sass-loader)
      // loads bootstrap's css.
      { test: /\.woff(\?v=\d+\.\d+\.\d+)?$/,   loader: "url?limit=10000&minetype=application/font-woff" },
      { test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/,  loader: "url?limit=10000&minetype=application/font-woff" },
      { test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,    loader: "url?limit=10000&minetype=application/octet-stream" },
      { test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,    loader: "file" },
      { test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,    loader: "url?limit=10000&minetype=image/svg+xml" },
      // Image loader. url-loader transforms image files. If the image size is smaller than 8192 bytes, it will be transformed into Data URL; otherwise, it will be transformed into normal URL.
      { test: /\.(png|jpg)$/, loader: 'url-loader?limit=8192' }
    ]
  },
  plugins: [

    new CommonsChunkPlugin('common', 'common.js'),
    // CommonsChunkPlugin will create react lib (and others from common chunk) as common.js, w/o this plugin react lib will be in bundle.js and common.js casue main.js require react
    new NoErrorsPlugin(),
    // NoErrorsPlugin doesn't create output if there is any error, more: http://webpack.github.io/docs/list-of-plugins.html#noerrorsplugin

    // Create landing pages for different apps, include build hash for cache purposes
    // Default file name is index.html
    new HtmlWebpackPlugin({ hash: true, chunks: ['common', 'main'] }),
    new HtmlWebpackPlugin({ filename: 'signup.html', hash: true, chunks: ['common', 'signup'] }),

    // Use provideplugin to load jQuery
    new ProvidePlugin({$: "jquery", jQuery: "jquery" }),

    // HtmlWebpackPlugin serve index.html template to output (./dist), more: https://www.npmjs.com/package/html-webpack-plugin
    new DefinePlugin({
      DEV: ENV_TYPE === 'development' || typeof ENV_TYPE === 'undefined',
      PRODUCTION: ENV_TYPE === 'production'
    })
    // DefinePlugin creates constants, more: http://webpack.github.io/docs/list-of-plugins.html#defineplugin
  ],
  resolve: {
    extensions: ['', '.js', '.jsx', '.scss'],
    modulesDirectories: ['node_modules', 'src']
    // More: http://webpack.github.io/docs/configuration.html#resolve-modulesdirectories
  },
  //devServer option is used to configure WebpackDevServer
  devServer: {
    contentBase: './dist', //content will be served from ./dist
    historyApiFallback: true
  },
  cache: true
  //Additionals config options
  // cache: boolean,
    // More: http://webpack.github.io/docs/configuration.html#cache
  // debug: boolean,
    // More: http://webpack.github.io/docs/configuration.html#debug
  // devtool: string,
    // More: http://webpack.github.io/docs/configuration.html#devtool
  // watch: boolean,
    // this is used as --watch flag at package.json as npm run watch command
    // “The dev server uses Webpack’s watch mode. It also prevents webpack from emitting the resulting files to disk. Instead it keeps and serves the resulting files from memory.” — This means that you will not see the webpack-dev-server build in your_file.js, to see and run the build, you must still run the webpack command.
  //More webpack conf options http://webpack.github.io/docs/configuration.html
}
