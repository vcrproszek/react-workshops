# frontendsandbox
Front end to develop new UI elements in it's own isolated repository

## Requirements
Make sure you have installed NodeJS and NPM first and that you can run them from the command line.
* Run `npm install` first to install dependencies

### To develop install:
* `npm install webpack -g`
* `npm install webpack-dev-server -g`

## Commands
* `npm run start` - Start the Webpack dev server 
* `npm run dev` - Start the Webpack dev server (with watch, HMR and more debug)
and go to the 'http://localhost:3000'

##Build production dist
* `npm run dist` - Build production files at `./dist`
