'use strict';

var config = require('./webpack.base.config.js');

config.module.loaders = config.module.loaders.concat([
  {
    test: /\.jsx?$/,
    loaders: ['babel-loader'],
    exclude: /node_modules/
  }
]);

module.exports = config;
